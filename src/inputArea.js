/**
 * Created by rashid_m on 14/2/18.
 */

class InputArea {
    constructor(config) {
        this.target = config.target;

        this.target.addEventListener('change',(obj)=>{
            // console.log(this.target);
            // console.log(obj);

            this.value = document.getElementById('inputArea').value;

            // console.log(this.value);

            document.dispatchEvent(new CustomEvent("inputChanged",{'detail':this.value}))
        })
    }

    run() {
        this.target.innerHTML = `
    <div><textarea id='inputArea' rows="10" cols="120" title="input-area"  placeholder="Paste input file content here..."></textarea>
    </div>
    `;
    }




}

export default InputArea