/**
 * Created by rashid_m on 14/2/18.
 */

class ButtonContainer {
    constructor(config) {
        this.target = config.target;
        this.buttons = new Set();

    }

    run() {
        this.target.innerHTML = `<div id="btnContainer"></div>`;
    }

    addBtn(key){
        this.buttons.add(key);
        this.container.appendChild(new Button(key).btnElem);
    }

    init(data){
        this.buttons.clear();
        this.container = document.getElementById('btnContainer');
        this.container.innerHTML = "";

        this.addBtn("All");

        for (let key in data) {
            this.addBtn(key);
        }

    }
}


class Button{

    constructor(id){
        let element = document.createElement("input");
        element.type = 'button';
        element.value = id;
        element.name = id;
        element.id = id;
        element.onclick = function () {
            document.dispatchEvent(new CustomEvent('buttonClicked',{'detail':id}))
        };

        this.btnElem = element;
    }
}

export default ButtonContainer