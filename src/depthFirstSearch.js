/**
 * Created by rashid_m on 14/2/18.
 */

class DFS {

    static findSingleCluster(data,key,name,isVisited = []){

        let services = Object.keys(data);

        for(let service of services){
            if(typeof isVisited[service] === 'undefiend')
                isVisited[service] = false;
        }

        let stack = [];
        let cluster = [];

        stack.push(key);

        while (stack.length>0) {
            let s = stack.pop();
            cluster.push(s);
            isVisited[s] = true;

            for(let cs of data[s]){
                if(!isVisited[cs])
                    stack.push(cs);
            }
        }

        return ({
            text: 'Cluster '+name+': '+cluster.join(", "),
            marker: isVisited
        });
    }


    static findAllCluster(data){
        let clusters = [];

        let services = Object.keys(data);

        let counter = 1;
        let retVal = DFS.findSingleCluster(data,services[0],counter);
        clusters.push(retVal.text);

        let isVisited = retVal.marker;

        let flag = true;
        while(flag){
            flag = false;
            for(let service of services){
                if(!isVisited[service]){
                    let retVal = DFS.findSingleCluster(data,service,++counter,isVisited);
                    clusters.push(retVal.text);
                    isVisited = retVal.marker;
                    flag = true;
                }
            }
        }

        return ({
            text: clusters
        });
    }
}

export default DFS