/**
 * Created by rashid_m on 14/2/18.
 */

class Title {
    constructor(config) {
        this.target = config.target;
    }

    run() {
        this.target.innerHTML =
    `
      <p>
        Program Cluster Detector
      </p>
    `;
    }
}

export default Title