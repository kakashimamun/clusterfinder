import css from './stylesheet.css'
import Title from './title'
import InputArea from './inputArea'
import DisplayArea from './displayArea'
import ButtonContainer from './buttonContainer'

import DFS from './depthFirstSearch'


class AppMain {

    constructor() {
        this.title = new Title({
            target: document.getElementsByTagName('heading')[0]
        });
        this.inputArea = new InputArea({
            target: document.getElementsByTagName('inputArea')[0]
        });

        this.buttonArea = new ButtonContainer({
            target: document.getElementsByTagName('buttonContainer')[0]
        });
        this.displayArea = new DisplayArea({
            target: document.getElementsByTagName('displayArea')[0]
        });

        this.serviceMesh = [];
    }

    run() {

        this.title.run();
        this.inputArea.run();
        this.buttonArea.run();
        document.addEventListener("inputChanged", (e) => {
            this.displayArea.clear();
            this.serviceMesh = [];
            this.loadData(e.detail);
        });

        document.addEventListener("buttonClicked",(e)=>{
            let key = e.detail;
            if(typeof this.serviceMesh[key] === 'undefined'){
                // TODO: DFS for all disjoint group;
                let retVal = DFS.findAllCluster(this.serviceMesh);
                this.displayArea.updateAsTable(retVal.text);
            }else {
                // TODO: DFS for starting from key
                let retVal = DFS.findSingleCluster(this.serviceMesh,key,key);
                this.displayArea.updateAsTable(retVal.text);

            }
        })
    }

    static initialize (parent, index, value) {
        if (typeof parent[index] === 'undefined') {
            parent[index] = value;
        }
        return parent[index];
    };

    loadData(data) {
        let res = data.split("\n");

        for (let i = 0; i < res.length; i++) {
            if(res[i].trim()){
                try {
                    let serviceInfo = res[i].split("<->", 2);
                    let service;
                    service = (serviceInfo[0]).trim();
                    AppMain.initialize(this.serviceMesh, service, new Set());
                    let connectedServices = serviceInfo[1].split(',');

                    for (let index in connectedServices) {
                        let conService = (connectedServices[index]).trim();
                        this.serviceMesh[service].add(conService);
                        AppMain.initialize(this.serviceMesh, conService, new Set());
                        this.serviceMesh[conService].add(service);
                    }
                }catch (e){
                    console.log(e);
                }

            }
        }

        this.buttonArea.init(this.serviceMesh);
    }
}


(new AppMain()).run();

