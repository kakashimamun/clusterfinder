/**
 * Created by rashid_m on 14/2/18.
 */

class DisplayArea {
    constructor(config) {
        this.target = config.target;
    }

    update(msg) {
        this.target.innerHTML = "";
        this.target.appendChild(document.createTextNode(msg));
    }


    updateAsTable(tableData) {
        let table = document.createElement('table');
        let tableBody = document.createElement('tbody');

        if (Array.isArray(tableData)) {
            tableData.forEach(function (rowData) {
                let row = document.createElement('tr');

                let cell = document.createElement('td');
                cell.appendChild(document.createTextNode(rowData));
                row.appendChild(cell);

                tableBody.appendChild(row);
            });
        } else {
            let row = document.createElement('tr');

            let cell = document.createElement('td');
            cell.appendChild(document.createTextNode(tableData));
            row.appendChild(cell);

            tableBody.appendChild(row);
        }

        table.appendChild(tableBody);

        this.target.innerHTML = "";
        this.target.appendChild(table);
    }


    clear(){
        this.target.innerHTML = "";
    }
}

export default DisplayArea